import javax.print.attribute.standard.JobHoldUntil;

public class App {
    public static void main(String[] args) throws Exception {
        Bat bat1 = new Bat("BOB");
        bat1.eat();
        bat1.sleep();
        bat1.fly();
        bat1.landing();
        bat1.takeoff();
        System.out.println("--------------------------------");
        Fish fish1 = new Fish("Nemo");
        fish1.eat();
        fish1.sleep();
        fish1.swim();
        System.out.println("--------------------------------");
        Plane plane1 = new Plane("Engle","001");
        plane1.fly();
        plane1.landing();
        plane1.takeoff();
        System.out.println("--------------------------------");
        Submarin submarin1 = new Submarin("Tu", "22500 million");
        submarin1.swim();
        System.out.println("--------------------------------");
        Bird bird1 = new Bird("lux");
        bird1.eat();
        bird1.sleep();
        bird1.walk();
        bird1.fly();
        bird1.landing();
        bird1.takeoff();
        System.out.println("--------------------------------");
        Snake snake1 = new Snake("Ethong");
        snake1.crawl();
        snake1.eat();
        snake1.sleep();
        snake1.walk();
        System.out.println("--------------------------------");
        Crocodie crocodie1 = new Crocodie("Jimmy");
        crocodie1.crawl();
        crocodie1.eat();
        crocodie1.sleep();
        crocodie1.walk();
        crocodie1.swim();
        System.out.println("--------------------------------");
        Rat rat1 = new Rat("Judy");
        rat1.eat();
        rat1.sleep();
        rat1.walk();
        rat1.run();
        System.out.println("--------------------------------");
        Human human1 = new Human("Boss");
        human1.eat();
        human1.sleep();
        human1.walk();
        human1.run();
        System.out.println("--------------------------------");
        Dog dog1 = new Dog("Dom");
        dog1.eat();
        dog1.sleep();
        dog1.walk();
        dog1.run();
        System.out.println("--------------------------------");
        Cat cat1 = new Cat("Zuza");
        cat1.eat();
        cat1.sleep();
        cat1.walk();
        cat1.run();
        System.out.println("--------------------------------");
        
        Flyable[] flyablesObjects = {bat1 , plane1,bird1};
        for (int i = 0;i < flyablesObjects.length;i++){
            flyablesObjects[i].fly();
            flyablesObjects[i].landing();
            flyablesObjects[i].takeoff();
        }
        System.out.println("--------------------------------");
        Walkable[] walkablesObjects = {human1 , rat1,dog1,cat1};
        for (int i = 0;i < walkablesObjects.length;i++){
            walkablesObjects[i].walk();
            walkablesObjects[i].run();
    }
}
}
