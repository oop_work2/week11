public abstract class Landanimal {
    private int numOfLeg;
    public Landanimal(int numOfLeg){
        this.numOfLeg = numOfLeg;
    }
    public int getNumOfLeg(){
        return numOfLeg;
    }
    public void setNumOfLeg(){
        this.numOfLeg = numOfLeg;
    }
    public abstract void walk();
    public abstract void run();

}
