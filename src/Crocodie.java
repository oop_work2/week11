public class Crocodie extends Animal implements Swimable,Crawlable{
    public Crocodie(String name){
        super(name, 4);
    }
    @Override
    public void crawl() {
        System.out.println(this + " crawl.");
        
    }

    @Override
    public void eat() {
        System.out.println(this + " eat.");
        
    }

    @Override
    public void sleep() {
        System.out.println(this + " sleep.");
        
    }

    @Override
    public void walk() {
        System.out.println(this + " walk.");
        
    }
    @Override
    public String toString(){
        return "Crocodie( " + getName() + " )";
    }
    @Override
    public void swim() {
        System.out.println(this + " swim.");
        
    }
}
