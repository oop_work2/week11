public class Submarin extends Vahicle implements Swimable{
    public Submarin(String name,String engineName){
        super(name, engineName);
    }

    @Override
    public void swim() {
        System.out.println(this + " swim.");
        
    }
    @Override
    public String toString() {
        return "Submarin( " + this.getName() + " ) engine : " + this.getEngineName();
    }
}
